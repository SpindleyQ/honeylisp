# Honeylisp

## Introduction
* 286 project
* Honeylisp vision

## Assembler
### What is an assembler?
* I _thought_ the hard part was going to be converting mnemonics to bytes
* Turns out the hard part is actually converting labels to bytes
* zero-page instructions are a different size!
### How it works
* Represent each opcode as a Fennel data literal
* nest blocks arbitrarily - "lexical scope"
* multi-pass

## VM
* Forth-ish stack machine
* "direct threaded" inner interpreter
* extend assembler with :vm directive
* "immediate words" are just Fennel functions

## Lite
* Minimal extensible text editor built in Lua
* love2d port

## Custom Editors
* imgui style
* show tile editor with map editor
* font + portrait editors based on tile editor
* generate bytes / code with fennel functions! (maps, gfx, etc)

## MAME Upload
* Nod to Dagen Brock's 2016 KFest talk on GSPlus https://www.youtube.com/watch?v=1LzCmpAanpE
* Integrated Jeejah networked REPL into MAME
* Can send arbitrary Fennel code to MAME to control it
* Poke blocks of memory over the network (nREPL uses bencode from bittorrent, which allows this to be fairly low overhead)

## Live Code Injection
* The assembled program is an object in memory, which we can extend interactively
* We can write new code and poke it into memory while the old code is running!
* Game code is a loop - we can have a "sync point" at the top of the loop where the state of the game is well-known
* (demo switching video modes, printing debug output, making sounds)

## Hot Reload
* Because the assembled program is an object in memory

## Tape generation
* Benefit of building tools in a game engine - I can just output audio
* Extended assembler to accept BASIC tokens and generate linked list of BASIC lines, so the whole thing could be bootstrapped

## Disk generation
* Take a ProDOS disk image, parse it, and add files to it
* Generate loader program, rest of game can be loaded as an overlay
* New disk image is generated on every build because why not? It's fast

## Neu] [ower
* Fun tricks: Random number generator (never used for gameplay purposes) = just dump a couple dozen random bytes

## 8-Bitsy
* Full "code-optional" environment
* Kind of awkward to actually use, but it works!
* Son drew some art
* Improvisational game design!


