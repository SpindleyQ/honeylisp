(local editor (require :editor))
(local util (require :lib.util))
(local lume (require :lib.lume))
(local link (require :link))
(require :link.command)
(local core (require :core))
(local command (require :core.command))
(local common (require :core.common))
(local keymap (require :core.keymap))
(local translate (require :core.doc.translate))
(local files (require :game.files))

(command.add nil {
  "honeylisp:open-project" (fn []
    (core.command_view:enter "Open Project"
      (fn [text item]
        (files.reload (or (and item item.text) text))
        (core.log "Opened"))
      (fn [text]
        (local files [])
        (each [_ item (pairs core.project_files)]
          (when (and (= item.type :file) (item.filename:find "^.*/game%.json"))
            (table.insert files item.filename)))
        (common.fuzzy_match files text))))})

(command.add #(link.machine:connected?) {
  "honeylisp:upload" (fn []
    (local p (util.reload "game"))
    (p:upload link.machine)
    (when link.machine.launch (link.machine:launch p))
    (core.log (string.format "%x" (p:lookup-addr p.start-symbol))))
  "honeylisp:reload" (fn []
    (local p-before (require :game))
    (local p (util.reload :game))
    (if (link.machine:hotswap p-before p)
      (core.log "Reloaded!")
      (core.log "Reload failed")))
})

(command.add #(and (link.machine:connected?) link.machine.overlay) {
  "honeylisp:vm-eval" (fn []
    (fn vm-eval [code]
      (local vmcode (fennel.eval (.. "[:vm " code "]") {:env _G :compiler-env _G}))
      (local prg (require :game))
      (local overlay (prg.vm:gen-eval-prg vmcode))
      (link.machine:overlay overlay)
      "")
    (editor.inline-eval vm-eval))
})

(command.add (fn [] true) {
  "honeylisp:rebuild" #(util.reload "game")
})

(fn selected-symbol []
  (local ldoc core.active_view.doc)
  (var (aline acol bline bcol) (ldoc:get_selection))
  (when (and (= aline bline) (= acol bcol))
    (set (aline acol) (translate.start_of_word ldoc aline acol))
    (set (bline bcol) (translate.end_of_word ldoc bline bcol)))
  (ldoc:get_text aline acol bline bcol))

(command.add "core.docview" {
  "fennel:eval" #(editor.inline-eval #(fv (fennel.eval $1 {:env _G :compiler-env _G}) {}))
  "lume:hotswap" (fn []
    (local modname
      (-> core.active_view.doc.filename
        (: :gsub "%.%a+$" "")
        (: :gsub "/" ".")
        (: :gsub "^data%." "")
        (: :gsub "%.init$" "")))
    (core.log (.. "Hotswapping " modname))
    (local (mod err) (util.hotswap modname))
    (when (not= err nil) (print err) (error err)))
  "honeylisp:address" (fn []
    (local word (selected-symbol))
    (local p (require "game"))
    (core.log (string.format "%s %x" word (or (p:lookup-addr word) -1)))
  )
})
(keymap.add {
  "alt+e" "fennel:eval"
  "alt+v" "honeylisp:vm-eval"
  "alt+r" "lume:hotswap"
  "alt+a" "honeylisp:address"
  "alt+l" "honeylisp:reload"
})

{}
