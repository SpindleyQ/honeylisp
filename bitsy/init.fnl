(local util (require :lib.util))
(local {: lo : hi : readjson} util)
(local tile (util.reload :game.tiles))
(local {: prg : vm : org : deflevel} (util.reload :bitsy.defs))
(local files (require :game.files))

(local disk (util.reload :bitsy.disk))

(util.reload :bitsy.gfx)
(util.reload :bitsy.footer)
(util.reload :bitsy.map)
(util.reload :bitsy.entity)
(util.reload :bitsy.player)
(util.reload :bitsy.boop)

(tile.appendtiles org.code)
(org.code:append [:align 0x100] :font)
(tile.appendgfx org.code files.game.font)
(tile.append-portraitwords vm)

(vm:var :tick-count)
(vm:word :handle-key :tick :read-key :player-key :hide-footer)
(vm:word :tick :map-specific-tick :tick-count :get 1 :+ :tick-count :set :player-redraw :rnd :drop)

(vm:var :next-level 0)
(vm:word :load-next-level :next-level :get :dup (vm:if [:load-level 0 :next-level :set] [:drop]))
(vm:word :load-level ; level-ptr --
  :lit :map-ptr :set :reload-level)

(vm:word :reload-level
  :map-player-yx :player-yx :set
  :map-specific-load
  :full-redraw)

(each [_ flag (ipairs (or files.game.flags []))]
  (vm:var (.. :cond-var- flag) vm.false)
  (vm:word (.. :cond- flag) (.. :cond-var- flag) :get))

(each [imap _ (ipairs files.game.levels)]
  (deflevel imap (.. :map imap)))

(vm.code:append :main
  [:jsr :reset]
  [:jsr :interpret]
  [:vm :hires
    :lit :map1 :load-level
    (vm:forever
      (vm:hotswap-sync :full-redraw)
      :interactive-eval-checkpoint
      :handle-key
    )
    :quit])

(disk.append-boot-loader prg)
(print "assembling")
(prg:assemble)
(print "assembled")
(disk.write prg)

prg
