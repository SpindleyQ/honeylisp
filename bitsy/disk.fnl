(local asm (require :asm.asm))
(local VM (require :asm.vm))
(local Prodos (require :asm.prodos))
(local util (require :lib.util))
(local {: lo : hi} util)
(local {: org} (require :bitsy.defs))

(fn append-boot-loader [prg]
  (local vm prg.vm)
  (set vm.code org.boot)
  (set prg.files [])

  (vm:word :loadfile ; length addr filename --
    0xbb00 :open :read :drop :close)
  (vm:word :loadscreen :cleargfx 0x2000 0x2000 :<rot :loadfile)

  (vm.code:append
    :boot
    [:jsr :reset]
    [:jsr :interpret]
    [:vm :hires]
    (when (util.file-exists "game/title.screen") [:vm (vm:pstr "TITLE.SCREEN") :loadscreen]))
  (each [addr _ (pairs prg.org-to-block)]
    (when (~= addr org.boot.org)
      (local filename (.. "STUFF." (length prg.files)))
      (table.insert prg.files {: filename :org addr})
      (vm.code:append [:vm :lit [:dw #(length (. prg.org-to-block addr :bytes))] addr :lit (.. :filename (length prg.files)) :loadfile])))
  (vm.code:append
    [:vm :native]
    [:jmp prg.start-symbol])
  (each [i file (ipairs prg.files)]
    (vm.code:append (.. :filename i) (Prodos.str file.filename))))

(fn org-copier [org]
  (local srclabel (.. :loader- org.addr))
  ; this will always copy full pages, because it simplifies the code and we don't actually care if a little extra
  ; garbage is tacked on to the end.
  ; We copy the pages in reverse order, because this allows us to safely move from 0x2000 to higher memory, and we
  ; never want to overlap with 0x2000 from lower memory, so either direction is safe
  (local dstpage-first (hi org.addr))
  (local dstpage-last (hi (+ org.addr (length org.bytes) -1)))
  [:block
    [:computed :srchi #(+ ($1:lookup-addr :ld-src) 2)]
    [:computed :dsthi #(+ ($1:lookup-addr :st-dst) 2)]
    [:computed :src-last #(+ ($1:lookup-addr srclabel) (* (- dstpage-last dstpage-first) 0x100))]
    [:computed :dst-last #(+ org.addr (* (- dstpage-last dstpage-first) 0x100))]
    [:ldx 0]
  :ld-src [:lda :src-last :x]
  :st-dst [:sta :dst-last :x]
    [:inx]
    [:bne :ld-src]

    [:lda :dsthi]
    [:cmp dstpage-first]
    [:beq :done]
    [:dec :srchi]
    [:dec :dsthi]
    [:bne :ld-src]
  :done])

(fn create-sys-loader [disk filename game]
  (local blocks [])
  (local prg (asm.new game))
  (local sys (prg:org 0x2000))
  (sys:append :loader-main)
  (set prg.start-symbol :loader-main)
  (sys:append (org-copier org.boot.block))
  (sys:append [:jmp :boot])
  (sys:append (.. :loader- org.boot.org) [:bytes org.boot.block.bytes])
  (prg:assemble)
  (disk:add-file (.. filename ".SYSTEM") Prodos.file-type.SYS 0x2000 sys.block.bytes))

(fn write [game]
  (local disk (Prodos "ProDOS_Blank.dsk"))
  (disk:update-volume-header {:name "EIGHTBITSY"})

  (create-sys-loader disk :BITSY game)

  (when (util.file-exists "game/title.screen")
    (disk:add-file "TITLE.SCREEN" Prodos.file-type.BIN 0x2000 (: (util.readjson "game/title.screen") :fromhex)))
  (each [_ file (ipairs game.files)]
    (disk:add-file file.filename Prodos.file-type.BIN file.org (. game.org-to-block file.org :bytes)))

  (disk:write "8Bitsy.dsk")
  disk)

{: write : append-boot-loader}

