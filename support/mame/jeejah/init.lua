local modname, modfile = ...
local modpath = modfile:sub(1, (-1 - modfile:reverse():find("/")))
print("PATH:", package.cpath)

if package.path:find("vendor/jeejah/") == nil then
  package.path = package.path .. ";" .. modpath .. "/../../../vendor/jeejah/?.lua" .. ";" .. modpath .. "/../../../vendor/?.lua"
  package.cpath = package.cpath .. ";" .. modpath .. "/../?.so" -- todo: windows / mac support?

  local fennel = require "fennel"
  fennel.path = './?.fnl;' .. modpath .. "/../../../vendor/jeejah/?.fnl"
  table.insert(package.loaders or package.searchers, fennel.make_searcher({correlate=true}))
end

local fennel = require "fennel"
local jeejah = require "jeejah.jeejah"
local exports = {}
function exports.startplugin()
    -- work around table that segfaults when you index a non-existent key
    getmetatable(emu.thread).__fennelview = function (self) return "<<emu.thread>>" end
    local coro = jeejah.start(7888, {fennel = true, debug = true})
    emu.register_periodic(function() coroutine.resume(coro) end)
    emu.register_stop(function() coroutine.resume(coro, "stop") end)
end

return exports
