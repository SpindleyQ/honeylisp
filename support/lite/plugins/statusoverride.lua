local core = require "core"

local get_items = core.status_view.get_items

core.status_view.get_items = function (self)
  if core.active_view and core.active_view.status_items then
    return core.active_view:status_items(self)
  else
    return get_items(self)
  end
end

