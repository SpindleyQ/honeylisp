# Honeylisp
## A hackable programming envrionment for the Apple II

Honeylisp is a set of integrated tools designed to allow tight iteration when
developing low-level 6502 code. It is written in Fennel, a Lisp dialect that
compiles to Lua. You program the Apple II by writing Fennel code that generates
6502 assembly as simple lists-of-symbols. You can then interactively update
that code while it runs with simple commands in the lite editor.

Honeylisp comprises:

  * A hackable programmer's text editor (lite)
    * Ported to the Love2D game engine framework (lite-hearted)
    * A simple imgui-inspired library for quickly building custom editors
  * An extensible 6502 assembler
  * A Forth-like stack-based virtual machine for the 6502
  * Deep MAME integration (jeejah)
    * Live interactive eval while your game is running
    * Hot code reload
    * TODO: snapshotting / rewind, rich symbols visible in the MAME debugger
  * Actual hardware integration
    * Supports uploading to the Apple II via the cassette port or a serial card
    * TODO: Live interactive eval and hot code reload

The main goal of the project is to support the creation of a game called Neut
Tower by its developer, but it is potentially generally interesting and useful
to others. Its design is focussed on the molding of the tool to your individual
needs; you are encouraged to fork it and modify any part to suit your taste.

## Getting Started

Right now, Honeylisp is developed solely on Linux and is not tested on MacOS or
Windows. You may have problems on those platforms.

Honeylisp assumes that you have LOVE2D and MAME installed on your system.
Install them from your distribution's package manager.

To check out the code and launch the editor, run the following commands from
a terminal:

```
$ git clone https://bitbucket.org/SpindleyQ/honeylisp.git
$ cd honeylisp
$ love .
```


