(local util (require :lib.util))
(local style (require :core.style))
(local {: defmulti : defmethod} (util.require :lib.multimethod))
(local {: textbutton} (util.require :editor.imstate))

(local inspector (util.hot-table ...))

(fn inspector.best-inspector [v]
  (var best-inspector nil)
  (var best-priority -1)
  (each [inspector {: priority : predicate} (pairs inspector.inspectors)]
    (when (and (> priority best-priority) (predicate v))
      (set best-inspector inspector)
      (set best-priority priority)))
  best-inspector)

(set inspector.inspect
  (defmulti (fn [state value view x y w]
              (when (= state.inspector nil)
                (set state.inspector (inspector.best-inspector value)))
              state.inspector) :inspect ...))

(fn inspector.register [name priority predicate inspect-func]
  (when (= inspector.inspectors nil)
    (set inspector.inspectors {}))
  (tset inspector.inspectors name {: predicate : priority :inspector inspect-func})
  (defmethod inspector.inspect name inspect-func))

(fn inspector.text-height [text ?font]
  (let [font (or ?font style.code_font)
        (_ newlines) (text:gsub "\n" "\n")]
    (* (font:get_height) (+ newlines 1))))

(fn inspector.draw-text [font text x y color]
  (renderer.draw_text font text x y color)
  (inspector.text-height text))

(inspector.register :default 0 #true (fn [state value view x y w]
  (inspector.draw-text style.code_font (fv value) x y style.text)))

(inspector.register :table 10
  #(and (= (type $1) :table) (not= (next $1) nil))
  (fn [state tbl view x y w]
    (local font style.code_font)
    (var h 0)
    ; todo: state assumes an .inspector key
    ; todo: inspector swapping
    ; todo: edit in place?
    (fn get-kstate [tbl k state]
      (when (= nil state.keys) (set state.keys {}))
      (when (= nil (?. state.keys k))
        (util.nested-tset state [:keys k] {:collapsed (= (type (. tbl k)) :table) :children {}}))
      (. state.keys k))
    (each [k v (pairs tbl)]
      (let [kstate (get-kstate tbl k state)
            kstr (fv k)
            wk (font:get_width kstr)
            xoffset (+ wk style.padding.x)
            toggle-collapse (textbutton view kstr x (+ y h))
            hv (if kstate.collapsed
                 (inspector.draw-text font "..." (+ x xoffset) (+ y h) style.syntax.comment)
                 (inspector.inspect kstate.children v view (+ x xoffset) (+ y h) (- w xoffset)))]
        (when toggle-collapse (set kstate.collapsed (not kstate.collapsed)))
        (set h (+ h hv style.padding.y))))
    h))

inspector.hot

