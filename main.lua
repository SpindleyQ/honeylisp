-- bootstrap the compiler
fennel = require("lib.fennel")
table.insert(package.loaders, fennel.make_searcher())
fv = fennel.view
pp = function(x) print(fv(x)) end
lume = require("lib.lume")
-- these set global variables and can't be required after requiring core.strict
_, luars232 = pcall(function () require("luars232") end)

_coroutine_resume = coroutine.resume
function coroutine.resume(...)
	local state,result = _coroutine_resume(...)
	if not state then
		error( tostring(result), 2 )	-- Output error message
	end
	return state,result
end

require("vendor.lite.main")
require("wrap")
