(local View (require :core.view))
(local tiles (require :game.tiles))
(local tiledraw (require :editor.tiledraw))
(local util (require :lib.util))
(local files (require :game.files))
(local {: attach-imstate : mouse-inside : activate : active? : button} (util.require :editor.imstate))

(local GraphicsEditView (View:extend))

(local sprite-scale 4)
(set GraphicsEditView.sprite-scale sprite-scale)

(fn GraphicsEditView.new [self]
  (GraphicsEditView.super.new self)
  (set self.tilecache (files.cache (self:resource-key)))
  (set self.itile 1)
  (set self.scrollheight math.huge)
  (set self.scrollable true)
  (attach-imstate self))
(fn GraphicsEditView.get_scrollable_size [self] self.scrollheight)
(fn GraphicsEditView.resource-key [self] :tiles)
(fn GraphicsEditView.tilesize [self] (values 16 16))
(fn GraphicsEditView.tilebytelen [self] (let [(w h) (self:tilesize)] (/ (* w h) 8)))
(fn GraphicsEditView.reload [self] (files.reload))
(fn GraphicsEditView.save [self] (files.save))

(fn GraphicsEditView.select-rel [self ditile]
  (when self.itile
    (local itile (+ self.itile ditile))
    (when (>= itile 1) (set self.itile itile))))

(fn GraphicsEditView.draw-sprite [self x y itile ?key]
  (love.graphics.draw (self.tilecache:sprite itile ?key) x y 0 self.sprite-scale self.sprite-scale))

(fn GraphicsEditView.draw-tile-selector [self x y w ?key]
  (var tilex x)
  (var tiley y)
  (var (pixw pixh) (self:tilesize))
  (set pixw (* (/ pixw 8) 7))
  (local tilew (* self.sprite-scale pixw))
  (local tileh (* self.sprite-scale pixh))
  (for [itile 1 (length self.tilecache.tiles)]
    (self:draw-sprite tilex tiley itile ?key)
    (when (and (= itile self.itile) (= ?key self.tilekey))
      (love.graphics.rectangle :line (- tilex 2) (- tiley 2) (+ tilew 4) (+ tileh 4)))
    (when (button self [:tile itile] tilex tiley tilew tileh)
      (set self.itile itile)
      (set self.tilekey ?key))
    (set tilex (+ tilex tilew 4))
    (when (>= (+ tilex tilew) (+ x w))
      (set tilex x)
      (set tiley (+ tiley tileh 4))))
  (+ tiley tileh (- y)))

GraphicsEditView
