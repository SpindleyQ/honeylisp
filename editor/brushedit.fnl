(local TileView (require :editor.tileedit))
(local tiledraw (require :editor.tiledraw))
(local tiles (require :game.tiles))
(local style (require :core.style))

(local BrushEditView (TileView:extend))

(fn BrushEditView.spritegen [self] tiledraw.char-to-sprite)
(fn BrushEditView.tilesize [self] (values 8 8))
(fn BrushEditView.tilekeys [self] [:gfx :mask])
(fn BrushEditView.map-bitxy [self x y] (values y x))
(fn BrushEditView.filename [self] "editor/brushes.json")
(fn BrushEditView.get_name [self] "Brush Editor")
(fn BrushEditView.draw-tile-flags [self x y])

BrushEditView

