(local util (require :lib.util))
(local fennel (require :lib.fennel))
(local style (require :core.style))
(local lume (require :lib.lume))
(local {: textbutton} (util.require :editor.imstate))
(local {: inspect} (util.require :inspector))
(local repl (util.hot-table ...))

(fn repl.inspector [{: vals : states} view x y]
  (var h 0)
  (each [i v (ipairs vals)]
    (set h (+ h (inspect (. states i) v view x (+ y h) view.size.x))))
  (+ h style.padding.y))

(fn repl.notify [listeners line]
  (each [_ listener (ipairs listeners)]
    (listener:append line)))

(fn repl.mk-result [vals]
  (local inspector #(repl.inspector $...))
  {:draw inspector : vals :states (icollect [_ (ipairs vals)] {})})

(fn repl.run [{: listeners}]
  (fennel.repl {:readChunk coroutine.yield
                :onValues #(repl.notify listeners (repl.mk-result $1))
                :onError (fn [errType err luaSource] (repl.notify listeners (repl.mk-result [err])))
                :pp #$1
                :env (lume.clone _G)}))

(fn repl.listen [{: listeners} listener]
  (table.insert listeners listener))

(fn repl.unlisten [{: listeners} listener]
  (lume.remove listeners listener))

(fn repl.submit [{: coro} chunk]
  (coroutine.resume coro (.. chunk "\n")))

(fn repl.new []
  (local result
    {:listeners []
     :listen #(repl.listen $...)
     :unlisten #(repl.unlisten $...)
     :submit #(repl.submit $...)
     :coro (coroutine.create repl.run)})
  (coroutine.resume result.coro result)
  result)

repl.hot
