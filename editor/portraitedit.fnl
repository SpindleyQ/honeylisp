(local util (require :lib.util))
(local TileView (require :editor.tileedit))
(local tiledraw (require :editor.tiledraw))
(local tiles (require :game.tiles))
(local {: textfield} (util.require :editor.imstate))

(local PortraitView (TileView:extend))

(fn PortraitView.tilesize [self] (values 32 32))
(fn PortraitView.tilekeys [self] [:gfx])
(fn PortraitView.resource-key [self] :portraits)
(fn PortraitView.map-bitxy [self x y]
  (local quadrant (+ (if (>= x 16) 2 0) (if (>= y 16) 1 0)))
  (local tilex
    (if (or (= x 0) (= x 30))  0
        (or (= x 1) (= x 31))  15
        (< x 16)               (- x 1)
                               (- x 15)))
  (local tiley (% y 16))
  (local (ibyte ibit) (PortraitView.super.map-bitxy self tilex tiley))
  (values (+ ibyte (* quadrant 32)) ibit))
(fn PortraitView.draw-tile-flags [self x y]
  (local tile (-?> self.tilecache.tiles (. self.itile)))
  (when tile
    (set tile.label (textfield self "Label" tile.label x (+ y 4) 100 200))))

(fn PortraitView.get_name [self] "Portrait Editor")

PortraitView
