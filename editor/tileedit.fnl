(local GraphicsEditView (require :editor.gfxedit))
(local style (require :core.style))
(local tiles (require :game.tiles))
(local files (require :game.files))
(local tiledraw (require :editor.tiledraw))
(local util (require :lib.util))
(local {: mouse-inside : activate : active? : checkbox : textfield} (util.require :editor.imstate))

(local TileView (GraphicsEditView:extend))

(set TileView.pixel-size 24)
(local pixel-size TileView.pixel-size)

(fn TileView.map-bitxy [self x y]
  (when (and (>= x 0) (< x 16) (>= y 0) (< y 16))
    (local ibyte (if (< x 8) y (+ y 16)))
    (local ibit
      (if (= x 0) 7
          (< x 8) (- x 1)
                  (- x 8)))
    (values ibyte ibit)))

(fn TileView.tilesize [self] (values 16 16))
(fn TileView.tilekeys [self]
  (if files.game.tilesets (icollect [_ key (pairs files.game.tilesets)] key)
    [:gfx]))

(fn get-byte [tile ibyte]
  (: (tile:sub (+ ibyte 1) (+ ibyte 1)) :byte))
(fn get-bit [tile ibyte ibit]
  (not= 0 (bit.band (get-byte tile ibyte) (bit.lshift 1 ibit))))
(fn set-bit [tile ibyte ibit is-set]
  (local orval (bit.lshift 1 ibit))
  (-> (get-byte tile ibyte)
    (bit.band (bit.bnot orval))
    (bit.bor (if is-set orval 0))))

(fn set-tile-bit [tile ibyte ibit is-set]
  (util.splice tile ibyte (string.char (set-bit tile ibyte ibit is-set))))

(fn draw-bit-color [bit x y]
  (local (bgcolor color) (tiledraw.pal-from-bit bit))
  (renderer.draw_rect x y pixel-size pixel-size bgcolor)
  (renderer.draw_rect (+ x 3) (+ y 3) (- pixel-size 6) (- pixel-size 6) color))

(fn draw-bit [bit x y even]
  (renderer.draw_rect x y pixel-size pixel-size (if bit [255 255 255] [0 0 0])))

(fn TileView.tile [self]
  (local (w h) (self:tilesize))
  (or (-?> self.tilecache.tiles (. self.itile) (. (or self.tilekey :gfx))) (string.rep "\0" (/ (* w h) 8))))

(fn TileView.draw-tile-editor [self tile x y]
  (when (not (active? self :tile))
    (set self.bit nil))
  (local (w h) (self:tilesize))
  (local editor-w (* (+ pixel-size 1) w))
  (local editor-h (* (+ pixel-size 1) h))
  (activate self :tile x y editor-w editor-h)
  (for [bitx 0 (- w 1)] (for [bity 0 (- h 1)]
    (local (ibyte ibit) (self:map-bitxy bitx bity))
    (local b (get-bit tile ibyte ibit))
    (local (px py) (values (+ x (* bitx (+ pixel-size 1))) (+ y (* bity (+ pixel-size 1)))))
    (if (= ibit 7)
      (draw-bit-color b px py)
      (draw-bit b px py (= (% bitx 2) 1)))
    (when (and (active? self :tile) (mouse-inside px py pixel-size pixel-size))
      (when (= self.bit nil) (set self.bit (not b)))
      (when (not= self.bit b)
        (self:update-tile (set-tile-bit tile ibyte ibit self.bit))))))
  (love.graphics.setColor 1 1 1 1)
  (values editor-w editor-h))

(fn TileView.draw-tile-flag [self flagname x y]
  (local flags (-?> self.tilecache.tiles (. self.itile) (. :flags)))
  (local flagset (if flags (. flags flagname) false))
  (let [(checked yNew) (checkbox self flagname flagset x y)]
    (when checked (tset flags flagname (if flagset nil true)))
    yNew))

(fn TileView.draw-tile-flags [self x y]
  (local tile (-?> self.tilecache.tiles (. self.itile)))
  (var y y)
  (when tile
    (set (tile.word y) (textfield self "Default word" tile.word x (+ y style.padding.y) (* 100 SCALE) (* 200 SCALE)))
    (set (tile.label y) (textfield self "Label" tile.label x (+ y style.padding.y) (* 100 SCALE) (* 200 SCALE))))
  (each [iflag flagname (ipairs (tiles.flags))]
    (set y (self:draw-tile-flag flagname x (+ y style.padding.y)))))

(fn TileView.update-tile [self newtile]
  (self.tilecache:update-tile self.itile newtile self.tilekey))

(fn TileView.draw [self]
  (self:draw_background style.background)
  (self:draw_scrollbar)
  (local (x y) (values (+ self.position.x style.padding.x (- self.scroll.x))
                       (+ self.position.y style.padding.y (- self.scroll.y))))
  (local (editor-w editor-h) (self:draw-tile-editor (self:tile) x y))
  (self:draw-tile-flags (+ x editor-w pixel-size) y)
  (var selector-y (+ y editor-h pixel-size))
  (each [_ key (ipairs (self:tilekeys))]
    (local selector-h (self:draw-tile-selector x selector-y (- self.size.x 20) key))
    (set selector-y (+ selector-y selector-h pixel-size)))
  (set self.scrollheight (- selector-y y)))

(fn TileView.resource-key [self] :tiles)
(fn TileView.get_name [self] "Tile Editor")

TileView
