(local util (require :lib.util))
(local TileView (require :editor.tileedit))
(local MapEditView (require :editor.mapedit))
(local ScreenEditView (require :editor.screenedit))
(local PortraitView (require :editor.portraitedit))
(local {: cmd-predicate} (util.require :editor.imstate))
(local core (require :core))
(local command (require :core.command))
(local keymap (require :core.keymap))
(local common (require :core.common))

(require :editor.8bitsy)
(require :presentation.commands)

(let [commands {}]
  (each [_ name (ipairs [:tile :portrait :font :brush :map])]
    (local cls (require (.. "editor." name "edit")))
    (tset commands (.. "honeylisp:" name "-editor") (fn []
      (local node (core.root_view:get_active_node))
      (node:add_view (cls)))))
  (command.add nil commands))

(local fileeditors
  {:screen {:view ScreenEditView :filefilter ".*%.screen"}})

(each [type {: view : filefilter} (pairs fileeditors)]
  (command.add nil
    {(.. "honeylisp:" type "-editor") (fn []
      (core.command_view:enter (.. "Open " type)
        (fn [text item]
          (local node (core.root_view:get_active_node))
          (node:add_view (view (or (and item item.text) text))))
        (fn [text]
          (local files [])
          (each [_ item (pairs core.project_files)]
            (when (and (= item.type :file) (item.filename:find filefilter))
              (table.insert files item.filename)))
          (common.fuzzy_match files text))))}))

(command.add (cmd-predicate :editor.gfxedit) {
  "graphics-editor:save"          (fn [] (core.active_view:save) (core.log "Saved"))
  "graphics-editor:reload"        (fn [] (core.active_view:reload) (core.log "Reloaded"))
  "graphics-editor:next-tile"     #(core.active_view:select-rel 1)
  "graphics-editor:previous-tile" #(core.active_view:select-rel -1)
})
(command.add (cmd-predicate :editor.tileedit) {
  "tileedit:copy"
  #(system.set_clipboard (: (core.active_view:tile) :tohex))
  "tileedit:paste"
  #(when (= (length (system.get_clipboard)) (* (core.active_view:tilebytelen) 2))
    (core.active_view:update-tile (: (system.get_clipboard) :fromhex)))
})
(keymap.add {
  "ctrl+s"    "graphics-editor:save"
  "alt+r"     "graphics-editor:reload"
  "left"      "graphics-editor:previous-tile"
  "right"     "graphics-editor:next-tile"
  "ctrl+c"    "tileedit:copy"
  "ctrl+v"    "tileedit:paste"
})

(command.add :editor.replview {
  "repl:submit" #(core.active_view:submit)
})

(local ReplView (require :editor.replview))
(local repl (require :editor.repl))
(command.add nil {
  "repl:create" (fn []
    (local node (core.root_view:get_active_node))
    (node:add_view (ReplView (repl.new)))
  )
})
(keymap.add {
  :return "repl:submit"
})

(fn inline-eval [eval]
  (let [ldoc core.active_view.doc
        (aline acol bline bcol) (ldoc:get_selection)
        inject #(ldoc:insert bline bcol (eval $1))]
    (if (and (= aline bline) (= acol bcol))
      (inject (ldoc:get_text aline 1 aline 10000000))
      (inject (ldoc:get_text aline acol bline bcol)))))

{: inline-eval}
