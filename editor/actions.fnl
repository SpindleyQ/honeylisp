(local util (require :lib.util))
(local {: defmulti : defmethod} (util.require :lib.multimethod))
(local {: textfield} (util.require :editor.imstate))

(local actions (util.hot-table ...))

(set actions.edit (defmulti #$1.action :edit ...))
(set actions.generate (defmulti #$1.action :generate ...))

(defmethod actions.edit :default (fn [action view x y w i] y))

(fn actions.register [key edit generate]
  (when (= actions.actionlist nil)
    (set actions.actionlist []))
  (lume.remove actions.actionlist key)
  (table.insert actions.actionlist key)
  (defmethod actions.edit key edit)
  (defmethod actions.generate key generate))

(fn actions.register-const [key generated-value]
  (actions.register key (fn [action view x y w i] y) #generated-value))

actions.hot
