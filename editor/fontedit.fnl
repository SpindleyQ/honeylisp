(local TileView (require :editor.tileedit))
(local tiledraw (require :editor.tiledraw))
(local tiles (require :game.tiles))
(local style (require :core.style))

(local FontEditView (TileView:extend))

(fn FontEditView.tilesize [self] (values 8 8))
(fn FontEditView.tilekeys [self] [:gfx])
(fn FontEditView.map-bitxy [self x y] (values y x))
(fn FontEditView.draw-tile-flags [self x y]
  (when self.itile
    (local char (string.char (+ self.itile 0x20 -1)))
    (renderer.draw_text style.big_font char x y style.text))
  (love.graphics.setColor 1 1 1 1))
(fn FontEditView.resource-key [self] :font)
(fn FontEditView.get_name [self] "Font Editor")

FontEditView

