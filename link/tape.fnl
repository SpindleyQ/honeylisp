(local tapegen (require :link.tapegen))
(local tape (require :asm.tape))

(local machine
  {:source (love.audio.newQueueableSource tapegen.samplerate 16 1 32)
   :play (fn [self sound] (self.source:queue sound))
   :stop (fn [self] (love.audio.stop))
   :upload
   (fn [self prg]
     (local (loader chunks) (tape.loader prg))
     (self:play (tapegen.gen-basic (. loader.org-to-block 0x801 :bytes)))
     (each [_ chunk (ipairs chunks)]
       (self:play (tapegen.gen-bin chunk))
     (love.audio.play self.source)))
   :connected? (fn [self] true)})

machine
