; using https://github.com/srdgame/librs232
(local (_ rs232) (pcall #(require :luars232)))
(local command (require "core.command"))

(fn check [err ...]
  (when (not= err rs232.RS232_ERR_NOERROR) (error (rs232.error_tostring err)))
  ...)

(fn open []
  (local port (check (rs232.open "/dev/ttyUSB0")))
  (port:set_baud_rate rs232.RS232_BAUD_9600)
  (port:set_data_bits rs232.RS232_DATA_8)
  (port:set_parity rs232.RS232_PARITY_NONE)
  (port:set_stop_bits rs232.RS232_STOP_1)
  (port:set_flow_control rs232.RS232_FLOW_HW)
  port)

(local machine
{:connect
 (fn [self]
   (when (not self.port)
     (set self.port (open))))
 :disconnect
 (fn [self]
   (when self.port
     (check (self.port:close))
     (set self.port nil)))
 :connected? (fn [self] self.port)
 :cmd
 (fn [self cmd]
   (check (self.port:write (.. cmd "\r"))
   (love.timer.sleep 0.08)))
 :write
 (fn [self addr bytes]
   (var bytes-to-write bytes)
   (var addrout addr)
   (while (> (length bytes-to-write) 0)
     (local bytesout (bytes-to-write:sub 1 10))
     (local hexbytes (bytesout:gsub "." (fn [c] (string.format "%02X " (string.byte c)))))
     (self:cmd (.. (string.format "%04X:" addrout) hexbytes))
     (set bytes-to-write (bytes-to-write:sub 11))
     (set addrout (+ addrout 10))))
 :monitor (fn [self] (self:cmd "CALL-151"))
;  :stub (fn [self org post-debug-stub]) ; todo
})

(command.add #(not (machine:connected?)) {
  "serial:connect" #(machine:connect)
})

(command.add #(machine:connected?) {
  "serial:disconnect" #(machine:disconnect)
  "serial:start-monitor" #(machine:monitor)
})

machine
