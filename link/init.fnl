(local link
  {:switch
   (fn [self name]
     (set self.machine (require (.. "link." name)))
     (set self.name name))
   :types [:serial :tape :mame]})

(local serial (require :link.serial))
(link:switch (if (and (pcall #(serial:connect)) (serial:connected?)) :serial :mame))

link
