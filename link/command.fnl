(local util (require :lib.util))
(local link (require :link))
(local editor (require :editor))
(local command (require :core.command))
(local keymap (require :core.keymap))

(each [_ linktype (ipairs link.types)]
  (command.add #(not= link.name linktype) {
    (.. "link:switch-to-" linktype) #(link:switch linktype)
  }))

(command.add #(and link.machine.run (not (link.machine:running?))) {
  "link:boot" #(link.machine:run)
})
(command.add #(and link.machine.die (link.machine:running?)) {
  "link:kill" #(link.machine:die)
})
(command.add #(not (link.machine:connected?)) {
  "link:connect" #(link.machine:connect)
})
(command.add #(link.machine:connected?) {
  "link:disconnect" #(link.machine:disconnect)
})

(let [connected-methods
      {:reboot :reboot
       :continue :continue
       :step :step
       :squelch :stop}]
  (each [name method (pairs connected-methods)]
    (command.add #(and (link.machine:connected?) (. link.machine method)) {
      (.. "link:" name) #(: link.machine method)
    })))

(command.add #(and (link.machine:connected?) link.machine.coro-eval) {
  "link:eval" #(util.in-coro #(editor.inline-eval #(link.machine:coro-eval $1)))
})

(keymap.add {
  "alt+m" "link:eval"
})

{}
