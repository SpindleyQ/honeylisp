(local util (require :lib.util))
(local core (require :core))
(local command (require :core.command))
(local keymap (require :core.keymap))
(local style (require :core.style))
(local SlideshowView (require :presentation.engine))

(fn set-scale [multiplier]
  (set _G.SCALE (* (love.graphics.getDPIScale) multiplier))
  (util.hotswap :core.style)
  (when (= multiplier 1)
    (set style.code_font (renderer.font.load (.. EXEDIR "/data/fonts/monospace.ttf") 15))))

(command.add nil {
  "presentation:start" (fn []
    (let [node (core.root_view:get_active_node)]
      (node:add_view (SlideshowView (util.reload :presentation.slides))))
  )
  "presentation:scale-up" #(set-scale 2)
  "presentation:restore-scale" #(set-scale 1)
})
(command.add :presentation.engine {
  "presentation:next" #(core.active_view:advance)
  "presentation:prev" #(core.active_view:back)
  "presentation:next-slide" #(core.active_view:next-slide)
  "presentation:prev-slide" #(core.active_view:prev-slide)
  "presentation:toggle-timer" #(core.active_view:toggle-timer)
  "presentation:reset-timer" #(core.active_view:reset-timer)
})
(keymap.add {
  "left"        "presentation:prev"
  "right"       "presentation:next"
  "ctrl+left"   "presentation:prev-slide"
  "ctrl+right"  "presentation:next-slide"
  "alt+t"       "presentation:toggle-timer"
})

