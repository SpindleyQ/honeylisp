(local util (require :lib.util))
(local lume (require :lib.lume))
(local {: parse} (util.require :presentation.engine))
(local core (require :core))
(local style (require :core.style))
(local TileEditView (require :editor.tileedit))
(local MapEditView (require :editor.mapedit))
(local PortraitEditView (require :editor.portraitedit))
(local FontEditView (require :editor.fontedit))
(local ScreenEditView (require :editor.screenedit))
(local files (require :game.files))
(local link (require :link))

(local h
  {:style true
   :font (renderer.font.load "presentation/font/PrintChar21.ttf" 64)
   :color style.caret
   :justify :center
   :topPadding 14
   :lowerPadding 64})
(local **
  {:style true
   :font (renderer.font.load "presentation/font/PRNumber3.ttf" 32)
   :color style.text
   :justify :left
   :lowerPadding 7
   :pause-after true})
(fn p [style ?text] (lume.merge style {:pause-after true} (if ?text {:text ?text :style false})))
(fn np [style ?text] (lume.merge style {:pause-after false} (if ?text {:text ?text :style false})))

(fn bgimg [filename] {:image filename :justify :center :overlay true :alpha 0.3 :topPadding 0})

(fn view-cleanup [view]
  (let [root core.root_view.root_node
        node (root:get_node_for_view view)]
    (when node (node:close_active_view root))))

(fn split-and-open [self f]
  (let [focused-view core.active_view
        focused-node (core.root_view:get_active_node)
        _ (when self.split (focused-node:split self.split))
        view (f self)
        node (core.root_view:get_active_node)]
    (when (= (core.root_view.root_node:get_node_for_view view) nil) (node:add_view view))
    (when self.split (core.set_active_view focused-view)) ; don't switch focus
    #(view-cleanup view)))

(fn openview [f ?extra] (lume.merge {:action #(split-and-open $1 f)} (or ?extra {})))
(fn openfile [filename ?extra]
  (openview #(let [ldoc (core.open_doc filename)
                   view (core.root_view:open_doc ldoc)]
               (when $1.line (view:scroll_to_line $1.line))
               view)
            ?extra))

(fn boot-game []
  (let [p (util.reload :game)]
    (util.in-coro (fn [] (link:switch :mame)
                         (link.machine:run)
                         (util.waitfor #(link.machine:connected?))
                         (p:upload link.machine)
                         (link.machine:launch p)))
    nil))

(fn vm-eval [...]
  (let [prg (require :game)
        overlay (prg.vm:gen-eval-prg [:vm ...])]
    (link.machine:overlay overlay)
    nil))

(parse [
  [h "" "" ""
     "Honeylisp"
     "" "" ""
   (np **) "Jeremy Penner"
           "https://spindleyq.itch.io/"
           "https://blog.information-superhighway.net/"
           "https://bitbucket.org/SpindleyQ/honeylisp"
           "https://gamemaking.social/@SpindleyQ"
           "https://twitter.com/SpindleyQ"
   {:pause-after true}
   {:target 30 :section :Intro}]
  [h  "Honeylisp is hard to explain"
   ** "It is an experimental programming environment designed to enable a productive Apple // game development workflow"
      "* Built with https://fennel-lang.org/"
      "* Extends the lite text editor https://github.com/rxi/lite"
      {:image "presentation/pics/assembly-markup.png" :justify :center}
      "* Built all tools from scratch from the assembler up"
      "* Not command-line driven - all tools, including the assembler / compiler, run _inside_ the editor"
      (openfile :presentation/slides.fnl {:split :right :line 89})
      "  * Including this presentation!"
   {:target 150}]
  [(bgimg "presentation/pics/boot-tower.jpeg")
   {:action #(files.reload :neuttower/game.json)}
   h  "Neu] [ower"
   ** "A small puzzle adventure game!"
      "Magic Trick #1: Assemble the game and poke it directly into emulated RAM"
      {:action boot-game}
      "--== D E M O ==--"
   {:target 240}]
  [h "Explain this voodoo!"
   ** "Directly inspired by Dagen Brock's 2016 KFest talk on GSPlus"
      "Ended up using MAME - Lua plugin system exposes EVERYTHING"
      "Use Jeejah nREPL server library with custom nREPL client"
      "The assembler running inside the editor means the output is a rich object, not a file"
    {:target 60}]
  [h "Hot-Code Reload"
   ** "What if I could preserve the current runtime state but rewrite the code?"
      (openfile :neuttower/level1.fnl {:split :right :line 59})
      "Magic Trick #2: Areas of memory can be marked as 'preserved' when new code is uploaded"
   {:target 180}]
  [(bgimg "presentation/pics/ggj2020.jpeg")
   h "Interactive Execution"
   ** "What if I could interactively try out new code while my game was running?"
   (np **) "Magic Trick #3"
      {:button #(vm-eval :mixed) :text ":mixed"}
      {:button #(vm-eval :hires) :text ":hires"}
      {:button #(vm-eval 1 2 :+ :.) :text "1 2 :+ :."}
      {:button #(vm-eval :jaye-yx :get :.) :text ":jaye-yx :get :."}
      {:button #(vm-eval :earthquake) :text ":earthquake"}
      {:pause-after true}
   {:target 180}]
  [h "The Tools"
   ** {:image "presentation/pics/retro-game-dev-quote.png" :justify :center :pause-after true}
      {:action #(files.reload :neuttower/game.json)}
      "14x16 tile editor"
      (openview #(TileEditView))
      "Font editor"
      (openview #(FontEditView))
      "Portrait editor"
      (openview #(PortraitEditView))
      "Map editor"
      (openview #(MapEditView))
      "Full-screen bitmap editor"
      (openview #(ScreenEditView :neuttower/title.screen) {:pause-after true})
   {:target 300 :section "Tooling"}]
  [h "Editing Editors With My Editor"
   ** "Lua provides a very dynamic environment"
      (openview #(MapEditView))
      (openfile :editor/mapedit.fnl {:split :right :line 235})
      "Downside:"
      {:image "presentation/pics/bsod.png" :justify :center :pause-after true}
   {:target 180}]
  [(bgimg "presentation/pics/bitsy.png")
   {:action #(files.reload :bitsy/game.json)}
   h "8-Bitsy"
   ** "Bitsy is a popular free, accessible, web-based game-making tool"
   {:action boot-game}
      "Spring Lisp Game Jam - 10 days to hack"
      "Could I make my tools a little less... programmer-y?"
      (openview #(MapEditView) {:pause-after true})
   {:target 180 :section "Branching Out"}]
  [h "Thanks!"
  (openfile :neuttower/level6.fnl {:split :right :line 164})
   (np **) "Questions?"
           {:topPadding 128}
           "Jeremy Penner"
           "https://spindleyq.itch.io/"
           "https://blog.information-superhighway.net/"
           "https://bitbucket.org/SpindleyQ/honeylisp"
           "https://gamemaking.social/@SpindleyQ"
           "https://twitter.com/SpindleyQ"
           {:pause-after true :section "Thanks!"}]
])

;   [(bgimg "presentation/pics/pete286.jpeg")
;    h "Some Background"
;    ** "2019: Built a 16-bit MS-DOS game engine, using only retro hardware and software."
;       " * Driven by a custom Forth interpreter"
;    {:target 90}]
;   [(bgimg "presentation/pics/ggj2020.jpeg")
;    h  "Neut Tower"
;    ** "2020: Created Neut Tower as part of two game jams.
; * Global Game Jam - One weekend - Feb 2020 - First two rooms
; * MS-DOS Game Jam - 1.5 months - April 2020 - 'Shareware Episode 1'"
;    {:target 60}]

;   [h "What is this unholy abomination?"
;    ** "Lisp and Forth?!"
;       {:image "presentation/pics/thinkhard.png" :justify :center}
;       "Not super keen on writing a complicated compiler"
;       " * \"Direct threaded\" inner interpreter"
;       "Forth allows efficient, composable, interactive code"
;    {:target 60}]

;   [h "Wait WTF Is An Assembler"
;    ** "It's just converting mnemonics to bytes, right?"
;       "Whoooops, actually the hard part is converting labels to addresses"
;       "Zero-page instructions are a different size, which messes up data layout!"
;       "Initial pass is needed to gather all symbols to determine sizes"
;    {:target 60}]

;   [h "Step 5: Running on Hardware"
;    ** "I have a IIgs with a serial cable - I can poke bytes in directly from the monitor"
;       "]IN#2\n]PR#2\n]CALL-151"
;       "Easy to send bytes faster than the monitor can process them"]
;   [h  "Audio"
;    ** "I have a II+ with a cassette port"
;       "LÖVE2D is a game engine - my editor can generate audio and play it back immediately"
;       "Need to generate a BASIC program to bootstrap my machine code"
;       (openfile :asm/tape.fnl {:split :right})
;       "  [:basic [10 :call :2061]]"
;       "Future work: Apple Game Server fastloader"]
;   [(bgimg "presentation/pics/beneath-apple-prodos.png")
;    h  "ProDOS"
;    ** "Disk image is a must-have for distribution"
;       (openfile :asm/prodos.fnl {:split :right :line 132})
;       "Of course I wrote my own disk image generation code!"
;       "Start with a blank ProDOS disk and add to it"
;       "Fun bugs!"
;       "* Accidentally implemented undelete instead of inserting new files at first"
;       "* Read the free space bitmap backwards and overwrote the OS"
;       "* Tried to name a volume starting with a number"]

