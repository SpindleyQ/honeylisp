(local {: deflevel : say : itile : controlstate} (require :neuttower.defs))
(local {: ev} (require :neuttower.entity))
(local tile (require :game.tiles))
(local files (require :game.files))
(local {: notes} (require :neuttower.boop))
(local {: walkable : neutable : debris : sittable} (tile.flag-to-bit))
(local level (deflevel 5 :level5))
(local vm level.vm)

(vm:word :snd-dropgarbage (notes [:a1] 0x02 0xf0))
(vm.code:append :debristiles)
(each [itile tiledef (ipairs files.game.tiles)]
  (when tiledef.flags.debris
    (vm.code:append [:db (tile.encode-itile itile)])))
(vm:word :randomgarbage :rnd 0x03 :& :lit :debristiles :+ :bget)

(vm:var :doortimer 0)
(vm:word :start-doortimer 0x10 :doortimer :set)
(vm:word :doortimer-tick
  :doortimer :get (vm:when
    :doortimer :get 1 :- :dup :doortimer :set
    :not (vm:when
      :lit :timedswitch ev.deact :entity>do)))

(vm:word :do-timedswitch
  ; only gord is physically able to touch it
  :dup ev.touch := (vm:when :start-doortimer :drop ev.act) :switch)

(vm:var :is-garbagerexx vm.false)
(vm:var :garbagerexx-yx 0x0710)
(vm:var :garbagerexx-introduced vm.false)
(vm:word :garbagerexx ; ev --
  :is-rexx? :swap :rexx :is-rexx? := :not (vm:when
    :is-rexx? (vm:if [
      :garbagerexx-yx :get :get-responder :get := (vm:when
        vm.true :is-garbagerexx :set
        :garbagerexx-introduced :get :not (vm:when
          (say :rexx "BO@oSSsS...,?  htgz")
          (say :rexx "I DON'T F3EL SO GOp0%foo)OD...>?qw" "idontfeelsogood")
          vm.true :garbagerexx-introduced :set
        ))
    ] [
      :is-garbagerexx :get (vm:when
        :get-responder :get :garbagerexx-yx :set
        vm.false :is-garbagerexx :set)
    ])))

(vm:word :not-picking-up? ; yxdest -- f
  debris :flag-at? :not)
(vm:word :can-drop-rubble? ; yxdest -- f
  :itile-at (itile :t-floor) :=
  :rexx-yx :get :itile-at (itile :t-floor) := :&)

(vm:word :move-garbagerexx ; yx -- f
  (vm:if-and [[:is-rexx?] [:is-garbagerexx :get]]
    [:dup 0xff :& 0x0d := (vm:if [
      (say :rexx "PARITYe#ERPORr(sbaitso" "  tellmeabout" "    your   problems") :drop vm.true :ret
     ] [
      (vm:if-and [[:dup :not-picking-up?] [:dup :can-drop-rubble?]]
        [:rexx-yx :get :randomgarbage :update-itile :snd-dropgarbage])
     ])])
  :move-noop)

(vm:word :move ; dir -- dir
  :dup :player-yx :get :yx+ :move-player-to 0x300 :snooze)
(vm:word :explode ; dir --
  :player-yx :get :yx+ :randomgarbage :update-itile :snd-explode)

(vm:word :explodingdoor
  :dup :door
  (vm:if-and [[ev.touch :=] [:is-jaye?] [:player-yx :get :get-responder :get :=] [:garbagerexx-yx :get :lit :south-rexx :get :=] [:gord-yx :get 0x812 :=]]
    [:rexx-yx :get
     :garbagerexx-yx :get :dup (itile :t-rexxstop) :update-itile :rexx-yx :set
     controlstate.rexx :controlstate :bset
     0x00ff :move :move :move
     (say :rexx "DAAAISYY" "   DAAAAAIIISYYYY" "d a i s y") :hide-footer
     :move :drop 0xff00 :move
     (say :rexx "GIVE ME" "  .,#YOUR ANSWEibmER" "      %$DOO00OOooo@'bell\"") :hide-footer
     :move :drop
     (say :rexx "UH OH") :hide-footer
     0xff00 :explode 0x0100 :explode 0x00ff :explode 0x0001 :explode 0 :explode
     0xffff :garbagerexx-yx :set
     controlstate.jaye :controlstate :bset
     :rexx-yx :set]))

(vm:var :healthyrexx-introduced vm.false)
(vm:word :healthyrexx ; ev --
  (vm:if-and [[:is-neut?] [:dup ev.touch :=] [:healthyrexx-introduced :get :not]]
    [(say :neut "REXX UNIT" "PERFORM FULL DIAGNOSTIC SCAN")
     (say :rexx "I'M IN TIP-TOP SHAPE, BOSS!")
     (say :neut "ACTIVATING RELIEF SUBROUTINE")
     vm.true :healthyrexx-introduced :set]) :rexx)

level
