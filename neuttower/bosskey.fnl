(local util (require :lib.util))
(local {: vm : prg : astr : style} (util.require :neuttower.defs))
(vm:word :boss-key :textmode :page2 (vm:until :read-key) :hires :page1)

; if we upload to page 2 we don't have to worry about clobbering screen holes
(local textorg (prg:org 0x0800))

(fn padding [s w style]
  (string.rep (astr " " style) (- w (length s))))
(fn pad [s w style]
  (.. s (padding s w style)))
(fn rpad [s w style]
  (.. (padding s w style) s))
(fn cellpad [s ?style]
  (local textstyle (or ?style style.normal))
  (match (type s)
    :nil (pad "" 9 textstyle)
    :string (pad (astr s textstyle) 9 textstyle)
    :number (rpad (astr (.. s " ") textstyle) 9 textstyle)
    :table (cellpad (. s 1) (. s 2))))
(fn cells [r a b c d]
  (.. (rpad (.. r "") 3 style.inverse)
      (cellpad a) (cellpad b) (cellpad c) (cellpad d)))

(fn generate-boss-screen-lines []
  [(-> (astr "A16  (L) TOTAL" style.inverse)
       (pad 38 style.inverse)
       (.. (astr "C!" style.inverse)))
   (.. (pad "" 38 style.inverse) (astr "24"))
   ""
   (cells "" ["    A" style.inverse] ["    B" style.inverse] ["    C" style.inverse] ["    D" style.inverse])
   (cells 1 "DEFINITEL" "Y REAL WO" "RK" "")
   (cells 2 "(NOT PLAY" "ING COMPU" "TER GAMES" ")")
   (cells 3)
   (cells 4 "" "HAMMERS" "BILLS" "SANDWICH")
   (cells 5 "JANUARY" 23 "$1" "CLUB")
   (cells 6 "FEBRUARY" 121 "$2" "REUBEN")
   (cells 7 "MARCH" 38 "$5" "BLT")
   (cells 8 "SMARCH" 97 "$10" "HOT DOG")
   (cells 9 "APRIL" 555 "$20" "I SAID IT")
   (cells 10 "WEDNESDAY" 246 "$50" "EGG SALAD")
   (cells 11 "KEYCODE" 1337 2757 9876)
   (cells 12 "NUMBERS" 12345 "$100" "IF I HAD")
   (cells 13 "LETTERS" "MARMOTS" "BENJAMIN" "100 I'D")
   (cells 14 "SYMBOLS" "^!@#%&?" "$$$$$" "EAT THEM")
   (cells 15)
   (cells 16 ["TOTAL" style.inverse] "TOO MANY" ["* MAGIC *" style.flashing] "ALL@ONCE")
   (cells 17) (cells 18) (cells 19) (cells 20)])

(fn bytes-from-lines [lines]
  (var bytes (string.rep (astr " ") 0x400))
  (each [y line (ipairs lines)]
    (local offset (+ (* (math.floor (/ (- y 1) 8)) 0x28)
                     (* (% (- y 1) 8) 0x80)))
    (set bytes (util.splice bytes offset line)))
  bytes)

(textorg:append [:bytes (bytes-from-lines (generate-boss-screen-lines))])

