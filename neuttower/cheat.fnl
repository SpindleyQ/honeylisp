(local {: vm : say-runon : say} (require :neuttower.defs))

(fn defcheat [name ...]
  (local cheatdata (.. name "-data"))
  (vm.code:append cheatdata [:db 0] [:bytes name] [:db 0])
  (vm:word name :lit cheatdata :cheatguard ...))

(vm:word :pcheatnext ; cheatdata -- pchar
  :dup :bget :inc :+)
(vm:word :reset-cheat ; cheatdata --
  0 :swap :bset)
(vm:word :cheatguard ; char cheatdata -- [optional rdrop]
  :dup :pcheatnext :bget :<rot := (vm:if ; cheatdata
    [:dup :bget :inc :over :bset
     :dup :pcheatnext :bget (vm:if [:drop :rdrop] [:snd-cheat :reset-cheat])]
    [:reset-cheat :rdrop]))

(defcheat :NTSPISPOPD :noclip :get :not :noclip :set)

(vm.code:append :level-pointers
  [:vm :level1 :level2 :level3 :level4 :level5 :level6])
(defcheat :NTXYZZY
  (say-runon :term "WARP TO ROOM #?" "(0 TO NOT CHEAT)")
  :read-digit :hide-footer (vm:if-and [[:dup 1 :>=] [:dup 7 :<]]
    [:dec :dup :+ :lit :level-pointers :+ :get :load-level]
    [:drop]))

(defcheat :NTCHUCK :chuck-mode :get :not :chuck-mode :set
  :chuck-mode :get (vm:if
    [(say :neut "CHUCK MODE ENABLED!" "* W H I N N Y *")]
    [(say :neut "CHUCK MODE DISABLED." "BEEP BOOP.")]))

(vm:word :cheat-key ; ascii --
  (vm:if-and [[:dup (string.byte "A") :>=] [:dup (string.byte "Z") :<=]]
    [:dup :NTSPISPOPD :dup :NTXYZZY :NTCHUCK]
    [:drop]))
