(local {: deflevel : say : itile} (require :neuttower.defs))
(local {: ev} (require :neuttower.entity))
(local level (deflevel 4 :level4))
(local vm level.vm)

(vm:word :term-dual-link
  :lit :term-exit :entity-itile (itile :termon) := (vm:if [:lit :term-exit] [:lit :term-scan]))

(vm:var :gord-sat vm.false)
(vm:word :tutorial-chair ; ev --
  ev.touch := (vm:when
    :transparent-entity-move :drop
    (vm:if-and [[:gord-sat :get :not] [:gord-sitting :get]]
      [vm.true :gord-sat :set
       (say :gord "PHEW, IT FEELS GOOD TO" "REST MY LEG FOR A BIT.")
       (say :gord "IF YOU NEED ME TO DO SOMETHING" "FROM MY CHAIR, YOU CAN PRESS" "THE Z KEY.")])))

level
