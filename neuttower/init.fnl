(local util (require :lib.util))
(local {: lo : hi : readjson} util)
(local tile (util.reload :game.tiles))
(local files (require :game.files))
(local {: prg : vm : org} (util.reload :neuttower.defs))

(local disk (util.reload :neuttower.disk))

(util.reload :neuttower.gfx)
(util.reload :neuttower.footer)
(util.reload :neuttower.map)
(util.reload :neuttower.entity)
(util.reload :neuttower.player)
(util.reload :neuttower.boop)
(util.reload :neuttower.cheat)

(tile.appendtiles org.code)
(org.code:append [:align 0x100] :font)
(tile.appendgfx org.code files.game.font)
(tile.append-portraitwords vm {:neut #[:vm :chuck-mode :get (vm:if [:lit :portrait-chuck] [:lit :portrait-neut])]})

(util.reload :neuttower.level1)
(util.reload :neuttower.level2)
(util.reload :neuttower.level3)
(util.reload :neuttower.level4)
(util.reload :neuttower.level5)
(util.reload :neuttower.level6)

(util.reload :neuttower.bosskey)

(vm:var :tick-count)
(vm:word :handle-key :tick :read-key :dup :cheat-key :player-key :hide-footer)
(vm:word :tick :map-specific-tick :tick-count :get 1 :+ :tick-count :set :player-redraw :rnd :drop)

(vm:var :next-level 0)
(vm:word :load-next-level :next-level :get :dup (vm:if [:load-level 0 :next-level :set] [:drop]))
(vm:word :load-level ; level-ptr --
  :lit :map-ptr :set :reload-level)

(vm:word :reload-level
  :map-jaye-yx :jaye-yx :set
  :map-neut-yx :neut-yx :set
  :map-gord-yx :gord-yx :set
  0 :gord-dir :set
  0xffff :rexx-yx :set
  :map-specific-load
  :full-redraw)

(vm.code:append :main
  [:jsr :reset]
  [:jsr :interpret]
  [:vm :hires
    :lit :level1 :load-level
    (vm:forever
      (vm:hotswap-sync :full-redraw)
      :interactive-eval-checkpoint
      :handle-key
    )
    :quit])

(disk.append-boot-loader prg)
(prg:assemble)
(disk.write prg)

prg
