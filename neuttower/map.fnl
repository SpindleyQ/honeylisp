(local {: lo : hi} (require :lib.util))
(local {: vm : mapw : maph : rot8l} (require :neuttower.defs))

(vm:def :lookup-flags ; itile -- flags
  [:lda vm.TOP :x]
  (rot8l 3) ; lllhhhhh > hhhhhlll
  [:adc #(lo ($1:lookup-addr :tileflags))]
  [:sta vm.W]
  [:lda #(hi ($1:lookup-addr :tileflags))]
  [:adc 0]
  [:sta vm.WH]
  [:ldy 0] [:lda [vm.W] :y]
  [:sta vm.TOP :x])

(vm:def :map-at ; yx -- pmap
  [:lda (- maph 1)]
  [:sec]
  [:sbc vm.TOPH :x]
  [:asl :a] ; x2
  [:asl :a] ; x4
  [:sta vm.TOPH :x]
  [:asl :a] ; x8
  [:asl :a] ; x16
  [:clc] [:adc vm.TOPH :x] ; x20
  [:adc vm.TOP :x]
  [:sta vm.TOP :x]
  [:lda :map-page]
  [:sta vm.TOPH :x])
(vm:word :itile-at ; yx -- itile
  :map-at :bget)

(vm:word :update-itile ; yx itile --
  :over :map-at :bset :drawtile-at)

(vm:word :drawtile-at ; yx --
  :dup :yx>screen :swap
  :itile-at :lookup-tile
  :drawtile)

