(local {: readjson} (require :lib.util))
(local {: deflevel : say : itile : controlstate} (require :neuttower.defs))
(local {: ev} (require :neuttower.entity))
(local {: decode-itile : encode-yx} (require :game.tiles))
(local files (require :game.files))
(local level (deflevel 1 :level1))
(local vm level.vm)

(let [map (. files.game.levels 1)
      maptiles map.map
      furniture-yx []]
  (for [ibyte 1 (length maptiles)]
    (let [btile (maptiles:sub ibyte ibyte)
          enctile (string.byte btile)
          itile (+ (decode-itile enctile) 1)
          mx (+ (% (- ibyte 1) 20) 1)
          my (- 12 (math.floor (/ (- ibyte 1) 20)))]
      (when (. files.game.tiles itile :flags :debris)
        (table.insert furniture-yx (encode-yx {:x mx :y my})))))
  (vm.code:append :furniture-yx)
  (for [_ 1 10]
    (let [ifurniture (math.random 1 (length furniture-yx))]
      (vm.code:append [:dw (. furniture-yx ifurniture)])
      (table.remove furniture-yx ifurniture))))

(vm:word :earthquake ; --
  :full-redraw
  :lit :furniture-yx
  10 (vm:for :rnd :shl4 0x7ff :& :snooze
             :dup :get :dup :itile-at 0x20 :+ :update-itile
             :snd-explode
             (vm:i) 9 := (vm:when (say :jaye "WOAH!") :hide-footer)
             2 :+) :drop
  0x1000 :snooze
  (say :jaye "THAT WAS AN EARTHQUAKE!"))

(vm:word :firstdoor
  (vm:if-and [[:is-jaye?] [:dup ev.touch :=] [:responder-itile (itile :doorclosed) :=]] [
    (say :jaye "IT WON'T OPEN!")
  ]) :door)

(vm:word :neutterm
  (vm:if-and [[:is-jaye?] [:dup ev.touch :=]] [
    :neut-hidden? (vm:if [
      (say :jaye "MAYBE NEUT CAN HELP.")
      controlstate.neut :controlstate :bset
      :move-to-responder
      controlstate.jaye :controlstate :bset
      (say :neut "NEUT V0.71.4RC12 ONLINE" "" "PRESS SPACE TO TAKE CONTROL")
    ] [
      (say :jaye "NEUT IS RUNNING NOW." "I CAN HIT THE SPACE BAR" "TO CONTROL THEM.")
    ])
  ]) :term)

(vm:word :firstterm
  (vm:if-and [[:is-jaye?] [:dup ev.touch :=]] [
    :responder-itile (itile :termoff) := (vm:when
      (say :jaye "LOOKS LIKE THERE'S STILL" "POWER TO THIS TERMINAL."))
    (say :jaye "IF I TURN A TERMINAL ON," "NEUT CAN USE IT TO" "TRAVEL THROUGH THE NETWORK.")
  ]) :term)

(vm:word :exitscanner
  (vm:if-and [[:is-jaye?] [:dup ev.touch :=]] [
    (say :jaye "IT'S A CARD SCANNER." "IT SHOULD OPEN THIS DOOR.")
    (say :jaye "IT'S NOT READING MY CARD" "FOR SOME REASON." "QUAKE MUST'VE DAMAGED IT.")
    (say :jaye "NEUT MIGHT BE ABLE TO" "HACK IT...")
  ]) :scan)

level
