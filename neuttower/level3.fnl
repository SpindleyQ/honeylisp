(local {: deflevel : say : itile : controlstate} (require :neuttower.defs))
(local {: ev} (require :neuttower.entity))
(local level (deflevel 3 :level3))
(local tile (require :game.tiles))
(local {: walkable : neutable : debris} (tile.flag-to-bit))

(local vm level.vm)

(vm:word :level3-load vm.true :gord-sitting :set)

(vm:var :gord-introduced vm.false)
(vm:word :flicker :get-responder ev.tog :entity>do 0x400 :snooze)
(vm:word :gordterm ; ev --
  (vm:if-and [[:is-neut?] [:dup ev.touch :=] [:gord-introduced :get :not]]
    [vm.true :gord-introduced :set
     (say :neut "HUMAN PRESENCE" "DETECTED")
     :flicker :flicker :flicker :flicker
     (say :neut "]HUMAN ASSISTANCE IS REQUIRED")
     (say :neut "]IF HUMAN IS PRESENT" " PLEASE RESPOND")
     :hide-footer :set-human-tileset :full-redraw
     :flicker :flicker :flicker :flicker
     (say :gord "WHAT THE...")
     (say :gord "IS SOMEONE IN THE TERMINAL?")
     :hide-footer :set-prog-tileset :full-redraw
     (say :gord "]HUMAN IS PRESENT")
     (say :neut "]GREETINGS, HUMAN")
     (say :neut "]THIS IS NEUT V0.71.4RC12")
     (say :neut "]PLEASE STATE NAME AND" " STATUS")
     (say :gord "]THIS IS GORD")
     (say :gord "V1, I GUESS.")
     (say :gord "]LEG IS PINNED UNDER DESK" " UNABLE TO MOVE")
     (say :neut "]CAN YOU REACH THE SWITCH" " BEHIND YOU?")
     0x400 :snooze :lit :gordswitch ev.act :entity>do 0x400 :snooze
     (say :gord "]I TURNED IT ON")
     (say :neut "]MY PROGRAMMER THANKS" " YOU, GORD")
     (say :neut "]WE WILL ASSIST YOU SOON")
     (say :gord "]AWAITING YOUR HELP, NEUT")
     :drop ev.noop])
   :term)

(vm:var :gord-jaye-met vm.false)
(vm:word :gordtable ; ev --
  ev.touch := (vm:when :transparent-entity-move
    (vm:if-and [[:is-jaye?] [:gord-jaye-met :get :not]]
      [vm.true :gord-jaye-met :set
       (say :jaye "HEY! GORD?" "I'M JAYE.")
       (vm:if
         [(say :gord "JAYE, AM I GLAD TO SEE YOU." "CAN YOU MOVE THIS DESK?")
          (say :jaye "LET ME TRY...")
          (say :jaye ". . . ." "!!!!.....")
          (say :jaye "!!!!!!!!!!!!...")
          (say :jaye "NO, I DON'T THINK I CAN.")
          (say :gord "I KEEP STARING AT THAT" "CLEANING ROBOT.")
          (say :gord "HE LOOKS LIKE HE COULD" "LIFT A BUILDING.")]
         [(say :gord "JAYE, AM I GLAD TO SEE YOU.")])]
      [:drop])))

(vm:var :rexx-introduced)
(vm:word :meetrexx ; ev --
  (vm:if-and [[:is-neut?] [:dup ev.touch :=] [:rexx-introduced :get :not]]
    [vm.true :rexx-introduced :set
     (say :neut "MOBILE ROBOTIC UNIT" "IDENTIFY YOURSELF")
     (say :rexx "HIYA BOSS!" "I'M REXX, THE JANITOR!")
     (say :rexx "AAAAAND YOUR NEW" "BEST FRIEND!!")
     (say :neut "A HUMAN IS IN PERIL")
     (say :neut "YOUR ASSISTANCE IS" "REQUIRED")
     (say :rexx "YOU NEED ME TO TAKE OUT" "SOME GARBAGE??")
     (say :rexx "OH BOY!! LET ME AT IT!")])
  :rexx)

(vm:word :floor-clear? 0x406 walkable :flag-at?)
(vm:word :meetgord ; ev --
  (vm:if-and [[ev.touch :=] [(itile :gord-ground) :responder-itile :=]]
    [:is-rexx? (vm:when
       (say :gord "AHHH NOOO" "NOT GARBAGE" "I AM NOT GARBAGE")
       (say :rexx "WHATEVER YOU SAY, BOSS!"))
     :is-jaye? (vm:when
       (say :jaye "ARE YOU HURT?")
       (say :gord "MY LEG WAS PINNED." "I DON'T THINK I CAN PUT" "ANY WEIGHT ON IT.")
       (say :jaye "HERE, LET ME HELP YOU UP.")
       (itile :t-floor) :set-respondertile
       0xff00 :gord-dir :set
       vm.false :gord-sitting :set
       controlstate.gord :controlstate :bset
       :get-responder :get :move-player-to
       controlstate.jaye :controlstate :bset
       (say :gord "THANKS.")
       (say :jaye "DON'T MENTION IT.")
       (say :jaye "I CAN HELP YOU GET AROUND IF" "YOU HELP ME NAVIGATE THIS" "MAZE OF A SECURITY SYSTEM.")
       (say :gord "I'M JUST AS EAGER TO GET" "OUT OF HERE AS YOU.")
       (say :gord "LET'S GO."))]))

level
