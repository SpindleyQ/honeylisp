(local asm (require :asm.asm))
(local util (require :lib.util))
(local lume (require :lib.lume))

(local tokens {})
    (each [i tok (ipairs [:end :for :next :data :input :del :dim :read :gr :text :pr# :in# :call :plot
                      :hlin :vlin :hgr2 :hgr :hcolor= :hplot :draw :xdraw :htab :home :rot= :scale=
                      :shload :trace :notrace :normal :inverse :flash :color= :pop :vtab :himem:
                      :lomem: :onerr :resume :recall :store :speed= :let :goto :run :if :restore
                      :& :gosub :return :rem :stop :on :wait :load :save :deffn :poke :print :cont
                      :list :clear :get :new :tab :to :fn :src :then :at :not :step :+ :- :* "/" ";"
                      :and :or :> := :< :sgn :int :abs :usr :fre :scrn :pdl :pos :sqr :rnd :log :exp
                      :cos :sin :tan :atn :peek :len :str$ :val :asc :chr$ :left$ :right$ :mid$])]
  (tset tokens tok (+ i 0x7f)))

(fn basic [prg]
  (fn parse-line [line]
    (local block [:block [:ref :nextline] [:dw (. line 1)]])
    (each [_ tok (ipairs (lume.slice line 2))]
      (table.insert block
        (if (= (type tok) :table) tok
            (. tokens tok) [:db (. tokens tok)]
          [:bytes (tostring tok)])))
    (table.insert block [:db 0])
    (table.insert block :nextline)
    block)
  (fn prg.dat-parser.basic [lines]
    (local block (prg.new-block))
    (each [_ line (ipairs (lume.slice lines 2))]
      (prg:parse-dats block [(parse-line line)]))
    (prg:parse-dats block [[:dw 0]])
    block)
  prg)

(fn loader [prg]
  (local lprg (basic (asm.new prg)))
  (local lorg (lprg:org 0x0801))
  (lorg:append [:basic [10 :call :2061]])
  (local generator
    {:chunks []
     :write
     (fn [self addr bytes]
       (local end (+ addr (bytes:len) -1)) ; end address inclusive
       (lorg:append
         [:lda #(util.lo addr)] [:sta :0x3c]
         [:lda #(util.hi addr)] [:sta :0x3d]
         [:lda #(util.lo end)]  [:sta :0x3e]
         [:lda #(util.hi end)]  [:sta :0x3f]
         [:jsr :0xfefd])
       (table.insert self.chunks bytes))})
  (prg:upload generator)
  (lorg:append [:jmp prg.start-symbol])
  (lprg:assemble)
  (values lprg generator.chunks))

{: basic : loader}
