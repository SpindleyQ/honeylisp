(local util (require :lib.util))
(local lume (require :lib.lume))
(local files (require :game.files))

(fn flags [] (or files.game.tileflags [:walkable]))
(fn flag-to-bit []
  (collect [iflag flag (ipairs (flags))] (values flag (bit.lshift 1 (- iflag 1)))))

(fn appendgfx [org gfx ?key ?label-prefix]
  (each [_ g (ipairs gfx)]
    (when g.label (org:append (.. (or ?label-prefix "") g.label)))
    (org:append [:bytes (. g (or ?key :gfx))])))

(fn appendtiles [org]
  (local tiles files.game.tiles)
  (local flag-lookup (flag-to-bit))
  (each [tileset key (pairs (or files.game.tilesets {:tileset :gfx}))]
    (org:append [:align 0x100] tileset)
    (appendgfx org tiles key (if (= key :gfx) nil (.. key :-))))
  (appendgfx org files.game.portraits nil :portrait-)
  (org:append :tileflags)
  (each [_ tile (ipairs tiles)]
    (var flags 0)
    (each [flag _ (pairs tile.flags)]
      (set flags (bit.bor flags (. flag-lookup flag))))
    (org:append [:db flags])))

(fn append-portraitwords [vm ?overrides]
  (local overrides (or ?overrides {}))
  (each [_ p (ipairs files.game.portraits)]
    (let [wordname (.. :draw-portrait- p.label)
          override (. overrides p.label)]
      (vm:word wordname :show-footer
        (if override (override p.label) [:vm :lit (.. :portrait- p.label)])
        :draw-portrait))))

(fn encode-yx [xy]
  (if xy (bit.bor (bit.lshift (- xy.y 1) 8) (- xy.x 1)) 0xffff))

(fn encode-itile [itile]
  (bit.bor
    (bit.lshift (bit.band (- itile 1) 0x07) 5)
    (bit.rshift (bit.band (- itile 1) 0xf8) 3)))

(fn decode-itile [enctile]
  (+ 1 (bit.bor
    (bit.lshift (bit.band enctile 0x1f) 3)
    (bit.rshift (bit.band enctile 0xe0) 5))))

(fn find-itile [tiles label ?itilenext]
  (local itile (or ?itilenext 1))
  (local tile (. tiles itile))
  (assert (not= tile nil) (.. "No such tile " label))
  (if (= tile.label label) (encode-itile itile)
    (find-itile tiles label (+ itile 1))))

{: appendtiles : appendgfx : append-portraitwords : flags : flag-to-bit : find-itile
 : encode-yx : encode-itile : decode-itile}

